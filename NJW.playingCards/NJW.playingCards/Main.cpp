#include <conio.h>
#include <iostream>

using namespace std;

enum Rank 
{
TWO = 2, THREE = 3, FOUR = 4, FIVE = 5, SIX = 6, SEVEN = 7, EIGHT = 8, NINE = 9, TEN = 10, JACK = 11, QUEEN = 12, KING = 13, ACE = 14
};

enum Suit 
{
DIAMONDS, HEARTS, SPADES, CLUBS
};


struct Card
{
	Suit suit;
	Rank rank;
};

void PrintCard(Card card)
{
	switch (card.rank)
	{
		case 2: cout << "Two"; break;
		case 3: cout << "Three"; break;
		case 4: cout << "Four"; break;
		case 5: cout << "Five"; break;
		case 6: cout << "Six"; break;
		case 7: cout << "Seven"; break;
		case 8: cout << "Eight"; break;
		case 9: cout << "Nine"; break;
		case 10: cout << "Ten"; break;
		case 11: cout << "Jack"; break;
		case 12: cout << "Queen"; break;
		case 13: cout << "King"; break;
		case 14: cout << "Ace"; break;
	}
	cout << " of ";

	switch (card.suit)
	{
		case 0: cout << "Diamonds"; break;
		case 1: cout << "Hearts"; break;
		case 2: cout << "Spades"; break;
		case 3: cout << "Clubs"; break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank) return card1;
	else return card2;
}


int main() 
{
	Card c1;
	c1.rank = TEN;
	c1.suit = HEARTS;
	PrintCard(c1);
	cout << "\n";

	Card c2;
	c2.rank = FOUR;
	c2.suit = DIAMONDS;
	PrintCard(c2);
	cout << "\n";


	Card c3 = HighCard(c1, c2);
	PrintCard(c3);
	cout << " is higher.";
	
	

	_getch();
	return 0;
}